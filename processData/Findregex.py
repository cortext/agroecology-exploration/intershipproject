import pandas as pd 
import unicodedata
import re 

#import the data CSV
data = pd.read_csv("/home/berta/intershipproject/Twitter/DataESFinalHastaga.csv")

#Loop for all the item in the data
for i in range(len(data)):
    #Get the text from the colum is needed
    text = data.get_value(i,'Caption')
    textNormalize = unicodedata.normalize('NFD', text)
    print(i)
    #The regex expression
    regexLink = r"(http?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|http?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})"
    regexHashtags = r"#(\w+)"
    #Put the text in a () for use the function sub
    text_str = (textNormalize)
    #The word for replace, the regex
    subst = ""
    #Look for the regex and replace it with the subst variable
    resultWithoutLink = re.sub(regexLink, subst, text_str, 0)
    #print("The new Line Without Link: ",resultWithoutLink)
    #Change the text with the new text
    text2_str = (resultWithoutLink)
    result1 = re.sub(regexHashtags, subst, text2_str, 0)
    #print("The new Line: ",result1)
    data['Caption'].loc[i] = result1

#Export the csv
export_csv = data.to_csv(r'/home/berta/intershipproject/Twitter/DataESFinalHastagaIT.csv', index = None, header=True)
