import spacy
from collections import Counter
import en_core_web_sm
import pandas as pd
import nltk
from nltk.corpus import stopwords
import json
from nltk.tokenize import word_tokenize 
nlp = en_core_web_sm.load()

nltk.download('stopwords')
nltk.download('punkt')

dfInfo = pd.read_csv("/home/berta/intershipproject/Twitter/DataESFinalHastaga.csv")
jsonF= []
for i in range(len(dfInfo)):
    title = dfInfo['tweet'][i]
    stop_words = set(stopwords.words('spanish'))
    word_tokens = word_tokenize(title)
    filtered_sentence =  ''
    for w in word_tokens: 
        if w not in stop_words: 
            filtered_sentence+=w
            filtered_sentence+=' '
    article = nlp(filtered_sentence)
    labels = [x.label_ for x in article.ents]
    print(i)
    jsonF.append(dict([(str(x), x.label_) for x in article.ents]))
    

with open('/home/berta/intershipproject/Twitter/NER.json','w') as output:
    json.dump(jsonF,output,sort_keys=True,indent=4, separators=(',', ': '))

