import json
import pandas as pd 


with open('/home/ordipret2/BerthaFiles/intershipproject/Twitter/NER.json') as json_file:
    dataNER = json.load(json_file)

NER = pd.DataFrame(columns = ['org','norp','person','fac','gpe','percent','cardinal','product','loc','money','time','date','ordinal','work_of_art','quantity','law','language','event','else'])

org = []
norp = []
person = []
fac = []
gpe = []
percent = []
cardinal = []
product = []
loc = []
money = []
time = []
date = []
ordinal = []
elses = []
work_of_art = []
quantity = []
law = []
language = []
event = []
for i in range(len(dataNER)):
    for key, value in dataNER[i].items():
        print(key, value)
        if(value == "ORG"):
            print(value)
            org.append(key)
        if(value == "NORP"):
            print(value)
            norp.append(key)
        if(value == "PERSON"):
            print(value)
            person.append(key)
        if(value == "FAC"):
            print(value)
            fac.append(key)
        if(value == "GPE"):
            print(value)
            gpe.append(key)
        if(value == "PERCENT"):
            print(value)
            percent.append(key)
        if(value == "CARDINAL"):
            print(value)
            cardinal.append(key)
        if(value == "PRODUCT"):
            print(value)
            product.append(key)
        if(value == "LOC"):
            print(value)
            loc.append(key)
        if(value == "MONEY"):
            print(value)
            money.append(key)
        if(value == "TIME"):
            print(value)
            time.append(key)
            quantity.append(key)
        if(value == "DATE"):
            print(value)
            date.append(key)
        if(value == "ORDINAL"):
            print(value)
            ordinal.append(key)
        if(value == "WORK_OF_ART"):
            print(value)
            work_of_art.append(key)
        if(value == "LAW"):
            print(value)
            law.append(key)
        if(value == "QUANTITY"):
            print(value)
            quantity.append(key)
        if(value == "EVENT"):
            print(value)
            event.append(key)
        if(value == "LANGUAGE"):
            print(value)
            language.append(key)
        else:
            print(value)
            elses.append(key)
    NER.loc[len(NER)] = [org,norp,person,fac,gpe,percent,cardinal,product,loc,money,time,date,ordinal,work_of_art,quantity,law,language,event,elses]

export_csv = NER.to_csv (r'/home/ordipret2/BerthaFiles/intershipproject/Twitter/NER_ES.csv', index = None, header=True)
