import pandas as pd 
import unicodedata
import re 


#import the data CSV
data = pd.read_csv("/home/ordipret2/BerthaFiles/intershipproject/Twitter/CleanDataENFinal.csv")

#Loop for all the item in the data
for i in range(len(data)):
    #Get the text from the colum is needed
    text = data.get_value(i,'Text Publication')
    print(text)
    #Normalize the text to have all the caracters in UT8
    textNormalize = unicodedata.normalize('NFD', text)
    print(str( textNormalize))
    #The regex expression
    regex = r"#(\w+)"
    #Put the text in a () for use the function sub
    text_str = (textNormalize)
    #The word for replace, the regex
    subst = ""
    #Look for the regex and replace it with the subst variable
    result = re.sub(regex, subst, text_str, 0)
    print("The new Line: ",result)
    #Change the text with the new text
    data['Text Publication'].loc[i] = result

#Export the csv   
export_csv = data.to_csv(r'/home/ordipret2/BerthaFiles/intershipproject/Twitter/Hashtags.csv', index = None, header=True)
