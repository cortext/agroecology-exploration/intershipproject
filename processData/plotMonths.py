from pylab import *
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

data = pd.read_csv("/home/ordipret2/BerthaFiles/intershipproject/youtube/byPeriod/espanol/semestres/SemestralPlot.csv")
data.head()

x = data['Quantity']
y = data['Text']
matplotlib.rcParams.update({'font.size': 18, 'font.family': 'STIXGeneral', 'mathtext.fontset': 'stix'})
index = np.arange(len(y))
plt.bar(index,x)
plt.xlabel('Period')
plt.ylabel('Videos')

plt.xticks(index,y, fontsize=12, rotation=90)
plt.show()
