from pandas import DataFrame
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import datetime

low_memory=False
plt.close('all')
data1 = pd.read_csv("/home/ordipret2/BerthaFiles/intershipproject/Facebook/MergeDataTotal.csv")
data = data1.drop_duplicates(['Text Publication'], keep='last')
data.head()

#Ver sin repetirse la cantidad de paginas que hay en el documento
publication_number = sorted(data['Author'].unique())
print(publication_number)
#Agrupar todo de acuerdo a cuantos hay por cada periodo de ano
group_by_date = data.groupby(by=['Author'])
#Cuenta y muesta en forma de tabla con respecto a todos los otros archivos
car_data_count = group_by_date.count()
print(car_data_count)
#Me da el numero de veces que aparece Lenuaje de acuerdo al Titulo
access_number = car_data_count['Text Publication']
print(len(access_number))
print(len(publication_number))

#Plotear en matplot con los siguientes parametros iniciales
#plt.figure(figsize=(30,5))
matplotlib.rcParams.update({'font.size': 12, 'font.family': 'STIXGeneral', 'mathtext.fontset': 'stix'})
index = np.arange(len(publication_number))
plt.bar(index, access_number)
plt.xlabel('Pages', fontsize=12)
plt.ylabel('Post', fontsize=12)
plt.xticks(index,publication_number, fontsize=6, rotation=90)
plt.show()