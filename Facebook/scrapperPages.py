from selenium import webdriver
import selenium
import pandas as pd 
import time 


driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')
#Set the information of the csv
#Get the csv that containt all the page url with the query
data = pd.read_csv("<<PAGES-ACCOUNT-CSV>>")
links = data['Link']
pageNames = data['Name']


#********* Login Function for enter to the Facebook and scrape the information
# Set the email and the password
def login():
    text_area_email = driver.find_element_by_id('m_login_email')
    text_area_email.send_keys("<<FACEBOOK-EMAIL>>")

    text_area_pass = driver.find_element_by_xpath('/html/body/div/div/div[3]/div/table/tbody/tr/td/div[3]/div[2]/form/ul/li[2]/div/input')
    text_area_pass.send_keys("<<FACEBOOK-PASSWORD>>")

    logIn_Button = driver.find_elements_by_xpath('/html/body/div/div/div[3]/div/table/tbody/tr/td/div[3]/div[2]/form/ul/li[3]/input')[0]
    logIn_Button.click()
    time.sleep(3)

###**********Extract Data from each Publication*****************
#This function is a scrapper for extract the imagen information, the text publication, quantity of likes, author and verify is there is comments in the publication 
# In case of use, verify first that each url is the correct because it can change. 
# I also put try/catch in case of the publication not have something.
def extractEachPublication(i):
    #Imagen information
    img = '/html/body/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/div[{}]/div[1]/div[3]/div[1]/a/img'.format(i)
    imagenContent = 'No Image'
    try:
        im = driver.find_element_by_xpath(img)
        imagenContent = im.get_attribute('alt')
        print(imagenContent)
    except:
        #In case of no Image
        print('noImages')
        pass
    #Text description of the post
    PostText = '/html/body/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/div[{}]/div[1]/div[2]/span'.format(i)
    text = 'No text'
    try:
        post = driver.find_element_by_xpath(PostText)
        text = post.text
        print(text)
    except:
        #In case of no text
        print('No Text')
        pass
    #Likes Quantity
    likesUrl = '/html/body/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/div[{}]/div[2]/div[2]/span[1]/a[1]'.format(i)
    likes = 'No likes'
    try:
        like = driver.find_element_by_xpath(likesUrl)
        likes = like.text
        print(likes)
    except:
        print('No likes')
        pass
    #Author of the publications
    a ='/html/body/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/div[{}]/div[1]/div[1]/table/tbody/tr/td[2]/div/h3/span/strong/a'.format(i)
    author=pageActual
    try:
        aut = driver.find_element_by_xpath(a)
        author = aut.text
        print(author)
    except:
        print('Author Problems')
        pass
    #Comments of the publications
    co = '/html/body/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/div[{}]/div[2]/div[2]/a[1]'.format(i)
    comm = driver.find_element_by_xpath(co)
    comment = comm.text
    print(comment)
    #Verify if there is comments
    #When there is comments you can see the number of comments there is
    if(comment == 'Comentar'):
        commets = [{'comments':'NoCommets'}]
    else:
        try:
            commets = getCommetsInfo(i)
            print(commets)
            time.sleep(3)
            driver.back()

        except: 
            pass
    publication.loc[len(publication)] = [links[0],author,text,linkActual,likes,commets,imagenContent]

###************Function for get the comments Data*****************
#This information take the indice of the publication, 
#Get comments, save it in the json object with the name of the person who comments and the messages it write.
def getCommetsInfo(c):
    co = '/html/body/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/div[{}]/div[2]/div[2]/a[1]'.format(c)
    next_Button = driver.find_elements_by_xpath(co)[0]
    next_Button.click()
    flag = True
    j = 1
    comments = []
   #It do it, how many time there is comments
    while flag:
        try:
            au = '/html/body/div/div/div[2]/div/div[1]/div[2]/div/div[5]/div[{}]/div/h3/a'.format(j)
            aut = driver.find_element_by_xpath(au)
            me = '/html/body/div/div/div[2]/div/div[1]/div[2]/div/div[5]/div[{}]/div/div[1]'.format(j)
            mes = driver.find_element_by_xpath(me)
            commentFormat = {
                'autor': aut.text,
                'comments': mes.text
            }
            print(commentFormat)
            comments.append(commentFormat)
        except:
            print('no more comments')
            flag= False
        j+=1
    return comments

#Main, whe have to pass over each page an scrappe it.
print(len(data))
primerLogin = True
#From each page open a new browser 
for pageIn in range(len(data)):
    publication = pd.DataFrame(columns = ['Page','Author','LinkPublication','Description','Likes','Comments','imagesInfo']) 
    linkActual = links[pageIn]
    pageActual = pageNames[pageIn]
    print(linkActual)
    driver.get(linkActual)
    #Login each time go inside
    if primerLogin:
        logIn_Button1 = driver.find_elements_by_xpath('/html/body/div/div/div[3]/div/div[1]/div[2]/a[2]')[0]
        logIn_Button1.click()
        login()
        time.sleep(2)
        primerLogin = False
    else:
        time.sleep(2)
    #Go over all the publication, extract the data and save it into csvFile
    flag = True
    while flag:
        try:
            for i in range(5):
                extractEachPublication(i+1)
            time.sleep(3)
            next_Button = driver.find_elements_by_xpath('/html/body/div/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/a')[0]
            next_Button.click()
        except:
            flag = False
            break
    np = r'<<OUTPUT-PATH>>/PagePublications{}.csv'.format(pageIn)
    export_csv = publication.to_csv(np, index = None, header=True)
    publication.empty
