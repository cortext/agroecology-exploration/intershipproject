from selenium import webdriver
import selenium
import pandas as pd 
import time 

#Open the web browser of Firefox
#Get the driver from https://github.com/mozilla/geckodriver/releases
driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')

#Login Function please Put you correct credentials
def login():
    text_area_email = driver.find_element_by_id('m_login_email')
    text_area_email.send_keys("<<FACEBOOK-EMAIL>>")

    text_area_pass = driver.find_element_by_xpath('/html/body/div/div/div[3]/div/table/tbody/tr/td/div[3]/div[2]/form/ul/li[2]/div/input')
    text_area_pass.send_keys("<<FACEBOOK-PASSWORD>>")

    logIn_Button = driver.find_elements_by_xpath('/html/body/div/div/div[3]/div/table/tbody/tr/td/div[3]/div[2]/form/ul/li[3]/input')[0]
    logIn_Button.click()
    time.sleep(3)

#Function that scrape all the page account related to the query. Get the link, the name, the type of page it is and the likes
def scraperPagedAccount(i):
    l = '/html/body/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div/div/div[{}]/div/table/tbody/tr/td[2]/a'.format(i+1)
    link_page = driver.find_element_by_xpath(l)
    link= link_page.get_attribute('href')
    print(link)

    n = '/html/body/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div/div/div[{}]/div/table/tbody/tr/td[2]/a/div[1]/div'.format(i+1)
    name_page = driver.find_element_by_xpath(n)
    name = name_page.text
    print(name)

    t = '/html/body/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div/div/div[{}]/div/table/tbody/tr/td[2]/a/div[2]/div/span'.format(i+1)
    type_page = driver.find_element_by_xpath(t)
    typ = type_page.text
    print(typ)

    li = '/html/body/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div/div/div[{}]/div/table/tbody/tr/td[2]/a/div[3]/div/span'.format(i+1)
    likes = 'No likes'
    try:
        likes_page = driver.find_element_by_xpath(li)
        likes = likes_page.text
        print(likes)
    except:
        #In case of no text
        print('No likes')
        pass
    pages.loc[len(pages)] = [name, link,typ,likes]

#Function that scrape all the "people" account related to the query. Get the link, the name, the type of page it is 
def scraperPeopleAccount(i):
    l = '/html/body/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div/div/div[{}]/table/tbody/tr/td[2]/a'.format(i+1)
    link_page = driver.find_element_by_xpath(l)
    link= link_page.get_attribute('href')
    print(link)

    n = '/html/body/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div/div/div[{}]/table/tbody/tr/td[2]/a/div[1]/div'.format(i+1)
    name_page = driver.find_element_by_xpath(n)
    name = name_page.text
    print(name)

    t = '/html/body/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div/div/div[{}]/table/tbody/tr/td[2]/a/div[1]/div'.format(i+1)
    type_page = driver.find_element_by_xpath(t)
    typ = type_page.text
    print(typ)
    people.loc[len(people)] = [name, link,typ]

#Facebook url
driver.get('https://mobile.facebook.com/home.php?ref_component=mbasic_home_header&ref_page=MFriendsCenterPYMKController')
login()
# Query, go directly to the reseach area bar and write you query.
# Please put your Query
text_area_pass = driver.find_element_by_xpath('/html/body/div/div/div[1]/div/form/table/tbody/tr/td[2]/input')
text_area_pass.send_keys("<<QUERY>>")
#Search Button to start the scrapper
search_Button = driver.find_elements_by_xpath('/html/body/div/div/div[1]/div/form/table/tbody/tr/td[3]/input')[0]
search_Button.click()
time.sleep(3)
#To select what we want to extract, pages, people, publications or etc
plus_Button = driver.find_elements_by_xpath('/html/body/div/div/div[2]/div[2]/div[1]/div[1]/div/a[2]')[0]
plus_Button.click()
time.sleep(2)
#The direction of the people and page, so we cant switch easyly 
people = '/html/body/div/div/div[2]/div/table/tbody/tr/td/ul/li[3]/table/tbody/tr/td/a'
pages = '/html/body/div/div/div[2]/div/table/tbody/tr/td/ul/li[6]/table/tbody/tr/td/a'
type_Button = driver.find_elements_by_xpath(pages)[0]
type_Button.click()
time.sleep(2)
#DataFrame for each selection
pages = pd.DataFrame(columns = ['Name','Link','Type','Likes']) 
people = pd.DataFrame(columns = ['Name','Link','Type']) 


flag = True
plusN_Button = driver.find_elements_by_xpath('/html/body/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/a')[0]
plusN_Button.click()
#Going into each page and extract the pages information
while flag:
    flag2 = True
    try:
        for i in range(12):
            scraperPagedAccount(i)
        plusN_Button = driver.find_elements_by_xpath('/html/body/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/a')[0]
        plusN_Button.click()
        time.sleep(2)
    except:
        flag = False
        break
export_csv = pages.to_csv (r'<<OUTPUT-PATH>>', index = None, header=True) 
