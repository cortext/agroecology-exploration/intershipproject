import pandas as pd
import json

data1 = []

for i in range(4):
    path ="/home/ordipret2/BerthaFiles/intershipproject/Universities/UNED/data{}.csv".format(i)
    print(path)
    data1.append(pd.read_csv(path,engine='python'))

df_row = pd.concat(data1,ignore_index=True,sort=True)
duple = df_row.drop_duplicates(['Name'], keep='last')
sort_by_Titulo = duple.sort_values('Name')
sort=True
print(sort_by_Titulo)

export_csv = sort_by_Titulo.to_csv (r'/home/ordipret2/BerthaFiles/intershipproject/Universities/UNED/dataTotal.csv', index = None, header=True)
