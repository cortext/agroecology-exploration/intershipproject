import pandas as pd
import json


path1 ="/home/ordipret2/BerthaFiles/intershipproject/Universities/UNED/dataTotal.csv"
path2 ="/home/ordipret2/BerthaFiles/intershipproject/Universities/resultsMergeAllUTitulo.csv"

print(path1,path2)
data1 = pd.read_csv(path1,engine='python')
data2 = pd.read_csv(path2,engine='python')

df_row = pd.concat([data1,data2],ignore_index=True,sort=True)
duple = df_row.drop_duplicates(['Titulo'], keep='last')
sort_by_Titulo = duple.sort_values('Titulo')
sort=True
print(sort_by_Titulo)

export_csv = sort_by_Titulo.to_csv (r'/home/ordipret2/BerthaFiles/intershipproject/Universities/resultsMergeAllUTitulo.csv', index = None, header=True)
