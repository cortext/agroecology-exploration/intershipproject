import requests,json
import pandas as pd


for j in range(3):
    pathEntry = '/home/berta/intershipproject/youtube/byPeriod/espanol/anual/20{}.json'.format(j+3)
    with open(pathEntry) as fileEntry:
        print(pathEntry)
        l = json.load(fileEntry)
        converJson = json.dumps(l)
        df = pd.read_json(converJson)
        pathOutput = r'/home/berta/intershipproject/youtube/byPeriod/espanol/anual/Anual{}.csv'.format(j+9)
        df.to_csv (pathOutput, index = None, header=True)