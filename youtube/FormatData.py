import requests,json
import pandas as pd
#Acces secret token
access_token = "AIzaSyD9Gd2rD5w6O72EmjdS1NOiz9zrM49vHwY"

#Function for manually add the statistics at each video, calling directly the API of Youtube
def get_statistics(video_id):
    url = 'https://www.googleapis.com/youtube/v3/videos'
    params = {'key':access_token,'id':video_id,'part':'contentDetails,statistics'}
    response=requests.get(url,params=params)
    data = response.json()
    return data
#Get the country channel, something that is an important and relevant data, so in case the channel has a country related,
# It's possible to call the API for this information

def get_countryChannel(channel_id):
    url = 'https://www.googleapis.com/youtube/v3/channels'
    pms = {'part':'id,snippet','id':channel_id,'key':access_token}
    res = requests.get(url,params=pms)
    data = res.json()
    print("chanelID",channel_id)
    country = data['items'][0]['snippet']['country']
    print("Country",country)
    return country

#So we will format our json file and delete some information we are no interesting on, and add some other we are really intersenting
for j in range(14):
    pathEntry = '/home/berta/intershipproject/youtube/byPeriod/espanol/semestres/agroecolog_a_OR_agroecologico_OR_agricultura_ecol_gica_OR_agro_ecolog_a_videos{}.json'.format(j+18)
    print(pathEntry)
    with open(pathEntry) as fileEntry:
        lst = []
        videosData = json.load(fileEntry)
        print(len(videosData))
        for i in range(len(videosData)):
            typeVideo = videosData[i]['id']['kind']
            print('VideoId',typeVideo)
            if(typeVideo=='youtube#video'):
                statics = 'No Data'
                contentDetails = 'No Data'
                country= 'NoCountryDefined'
                """video_stats = get_statistics(videosData[i]['id']['videoId'])
                if(len(video_stats['items']) !=0):
                    statics = video_stats['items'][0]['statistics']
                    contentDetails = video_stats['items'][0]['contentDetails']
                
                try:
                    country = get_countryChannel(videosData[i]['snippet']['channelId'])
                except:
                    print('No country defined')
                    pass"""
                results_json = {
                    'channelTitle': videosData[i]['snippet']['channelTitle'],
                    'channelid':videosData[i]['snippet']['channelId'],
                    'description':videosData[i]['snippet']['description'],
                    'country': country,
                    'title':videosData[i]['snippet']['title'],
                    'publishedAt':videosData[i]['snippet']['publishedAt'],
                    'statistics': statics,
                    'contentDetails': contentDetails,
                    'id': videosData[i]['id']['videoId']
                    }
                lst.append(results_json)
            else:
                print('There is something like playlist or channel')
        #print(lst)
        converJson = json.dumps(lst,indent=4, separators=(',', ': '))
        df = pd.read_json(converJson)
        pathOutput = r'/home/berta/intershipproject/youtube/byPeriod/espanol/semestres/Semestral{}.csv'.format(j+18)
        df.to_csv (pathOutput, index = None, header=True)
