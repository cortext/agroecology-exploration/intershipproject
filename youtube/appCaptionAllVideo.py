import requests,json
import pandas as pd

for j in range(5):
    pathCSV = "/home/berta/intershipproject/youtube/byPeriod/espanol/anual/Anual{}.csv".format(j+7)
    print(pathCSV)
    pathCaption = '/home/berta/intershipproject/youtube/byPeriod/espanol/anual/Caption{}.json'.format(j)
    print(pathCaption)
    dfInfo = pd.read_csv(pathCSV)
    with open(pathCaption) as file1:
        jdata = json.load(file1)
        dfCaption = pd.DataFrame(jdata)
        lstCaption = ['NoCaption'] * (len(dfInfo))
        for j in range(len(dfInfo)):
            for g in range(len(dfCaption)):
                if(dfInfo['id'][j] == dfCaption['videoId'][g]):
                    print("found it",dfInfo['id'][j],dfCaption['videoId'][g])
                    lstCaption[j] = dfCaption['text'][g]
                    
        print(len(dfInfo),len(lstCaption))
        dfInfo['Caption'] = lstCaption
        dfInfo.to_csv( pathCSV, index = None, header=True)