import requests,json
import pandas as pd
access_token = "AIzaSyBMA29t68D2gYF0nEhOrtdrxJ8Cv3EjhCg"
query = 'агроэкологии'
url = 'https://www.googleapis.com/youtube/v3/search'

full_data = []
page = ''

with open("data.json","a") as output:
    def get_statistics(video_id):
        url = 'https://www.googleapis.com/youtube/v3/videos'
        params = {'key':access_token,'id':video_id,'part':'contentDetails,statistics'}
        response=requests.get(url,params=params)
        data = response.json()
        return data
    flag = True
    while flag:
        params = {'type':'video','key':access_token,'q':query,'part':'snippet','order':'viewCount','pageToken':page}
        res = requests.get(url,params=params)
        print("Connection status: %s"%res)
        print(res.ok)
        data = res.json()
        if(res.ok):
            flag = True
        else:
            flag = False
            break
        full_data.extend(data['items'])
        lst = []
        print(len(data['items']))
        for video in data['items']:
            print(video)
            video_stats = get_statistics(video['id']['videoId'])
            print(str(video_stats))
            if(video_stats):
                results_json = {
                'channelTitle': video['snippet']['channelTitle'],
                'title':video['snippet']['title'],
                'description':video['snippet']['description'],
                'publishedAt':video['id']['publishedAt'],
                'statistics': video_stats['items'][0]['statistics'],
                'contentDetails': video_stats['items'][0]['contentDetails'],
                'id': video_stats['items'][0]['id']}
                json.dump(results_json,output,sort_keys=True, indent=4, separators=(',', ': '))
                
        try:
            page = data['nextPageToken']
        except:
            break
   