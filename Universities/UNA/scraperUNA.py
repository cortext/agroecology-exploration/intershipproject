from selenium import webdriver
import selenium
import pandas as pd 
import time 

driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')


papers = pd.DataFrame(columns = ['Periodo','Titulo','autor','formato','link'])
time.sleep(2)
def scrapper(u):
    n = '/html/body/table[5]/tbody/tr[{}]/td[5]/a'.format(u+2)
    nombre = driver.find_element_by_xpath(n)
    print(nombre.text)
    p = '/html/body/table[5]/tbody/tr[{}]/td[6]'.format(u+2)
    periodo = driver.find_element_by_xpath(p)
    print(periodo.text)
    a = '/html/body/table[5]/tbody/tr[{}]/td[3]'.format(u+2)
    autor = driver.find_element_by_xpath(a)
    print(autor.text)
    f = '/html/body/table[5]/tbody/tr[{}]/td[4]'.format(u+2)
    formato = driver.find_element_by_xpath(f)
    print(formato.text)
    n = '/html/body/table[5]/tbody/tr[{}]/td[10]/table/tbody/tr/td/a/img'.format(u+2)
    f = '/html/body/table[5]/tbody/tr[{}]/td[10]/br'.format(u+2)
    l = '/html/body/table[5]/tbody/tr[{}]/td[5]/a'.format(u+2)
    link = driver.find_element_by_xpath(l)
    print("link",link.get_attribute('href'))
    try:
        button = driver.find_element_by_xpath(l)
        button.click()
        time.sleep(2)
        peri = driver.find_element_by_xpath('/html/body/table[6]/tbody/tr[12]/td[2]/a')
        periodo = peri.text
        print("periodo",periodo)
        driver.execute_script("window.history.go(-1)")
        time.sleep(2)
    except selenium.common.exceptions.NoSuchElementException:
        pass
    papers.loc[len(papers)] = [periodo,nombre.text,autor.text,formato.text,link.get_attribute('href')]

g = 151
while(g <=460):
    url = 'http://www.opac.una.ac.cr/F/MJUN83KBDKPPX8NSY1UQHNPT5443GKG1FGAK4A6VG54NYMDSCA-09468?func=short-jump&jump={}'.format(g)
    driver.get(url)
    time.sleep(1)
    for i in range(9):
        scrapper(i)
    g=g+10

export_csv = papers.to_csv (r'/home/ordipret2/BerthaFiles/proyectoData/Universities/UNA/try1.csv', index = None, header=True) 