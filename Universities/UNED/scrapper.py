from selenium import webdriver
import selenium
import pandas as pd 
import time 
import re

driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')

driver.get('https://primo-tc-na01.hosted.exlibrisgroup.com/primo-explore/search?vid=UNED&lang=es_ES')
papers = pd.DataFrame(columns = ['Name','Author','formato','link'])
time.sleep(4)

def scraper(u):
    print(u+1)
    n = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[1]/prm-search-result-list/div/div[1]/div/div[{}]/prm-brief-result-container/div[1]/div[3]/prm-brief-result/h3/a'.format(u+1)
    ln = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[1]/prm-search-result-list/div/div[1]/div/div[{}]/prm-brief-result-container/div[1]/div[3]/prm-brief-result/h3/a/span/prm-highlight/span'.format(u+1)
    a = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[1]/prm-search-result-list/div/div[1]/div/div[{}]/prm-brief-result-container/div[1]/div[3]/prm-brief-result/div[1]/span/span[2]/prm-highlight/span'.format(u+1)
    f = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[1]/prm-search-result-list/div/div[1]/div/div[{}]/prm-brief-result-container/div[1]/div[3]/div[1]/span'.format(u+1)    
    print(n)
    print(ln)
    nameT = ''
    linkText = ''
    authorT = ''
    formatoT = ''
    try:
        name = driver.find_element_by_xpath(ln)
        nameT = name.text
        print(nameT)
    except:
        print('no name')
        pass
        
    try:
        link = driver.find_element_by_xpath(n)
        linkText = link.get_attribute('href')
        print(linkText)
    except:
        print('no link')
        pass
    try:
        autor = driver.find_element_by_xpath(a)
        authorT = autor.text
        print(authorT)
    except:
        print("no author")
        pass   
    try:
        formato = driver.find_element_by_xpath(f)
        formatoT = formato.text
        print(formatoT)
    except:
        print("no type")
        pass
    time.sleep(1)
    papers.loc[len(papers)] = [nameT,authorT,formatoT,linkText]
    

tf = '//*[@id="searchBar"]'      
textF = driver.find_element_by_xpath(tf)
QueryField = textF.send_keys('agroecologia')
bn = '/html/body/primo-explore/div/prm-explore-main/div/prm-search-bar/div[1]/div/div[2]/div/form/div/div/div[3]/button[2]'
selecB = driver.find_element_by_xpath('//*[@id="select_value_label_7"]')
selecB.click()
Biblio = driver.find_element_by_xpath('//*[@id="select_option_11"]')
Biblio.click()
searchButton = driver.find_element_by_xpath(bn)
searchButton.click()
print("entramos en la busqueda")
time.sleep(4)
ind = driver.find_element_by_xpath('/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[1]/prm-search-result-list/div/prm-search-result-tool-bar/md-toolbar/div[2]/span[2]')
index = ind.text
print('# files',index)
indexN = re.search('[0-9]*',index)
print(indexN.group())
for i in range(int(indexN.group())):
    print('index',i)
    if(i!=0):        
        if(((i)%10)==0):
            nex = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[1]/prm-search-result-list/div/div[1]/div/div[{}]/button'.format(i+1)
            nextButton = driver.find_element_by_xpath(nex)
            nextButton.click()
            print("next botton")
            scraper(i)
        else:
            scraper(i)
    else:
        scraper(i)
export_csv = papers.to_csv (r'/home/ordipret2/BerthaFiles/intershipproject/Universities/UNED/data3.csv', index = None, header=True) 