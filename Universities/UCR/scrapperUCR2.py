from selenium import webdriver
import selenium
import pandas as pd 
import time 

driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')

papers = pd.DataFrame(columns = ['Titulo','link','autor','Periodo','formato'])
time.sleep(4)
def scraper(u):
    print(u+1)
    n = '/html/body/div[2]/div[2]/div[1]/div/div[{}]/div[2]/a'.format(u+1)
    print(n)
    nombre = driver.find_element_by_xpath(n)
    print(nombre.text)
    p = '/html/body/div[2]/div[2]/div[1]/div/div[{}]/div[3]'.format(u+1)
    periodo = ''
    try:
        period = driver.find_element_by_xpath(p)
        periodo = period.text
        print(period.text)
    except selenium.common.exceptions.NoSuchElementException:
        pass
    a = '/html/body/div[2]/div[2]/div[1]/div/div[{}]/div[1]'.format(u+1)
    autor = driver.find_element_by_xpath(a)
    print(autor.text)
    l = '/html/body/div[2]/div[2]/div[1]/div/div[{}]/div[2]/a'.format(u+1)
    link = driver.find_element_by_xpath(l)
    print(link.get_attribute('href'))
    papers.loc[len(papers)] = [nombre.text,link.get_attribute('href'),autor.text,periodo,'Articulo']

for j in range(2):
    url = 'https://revistas.ucr.ac.cr/index.php/index/search/search?query=agroecologia&searchJournal=&authors=&title=&abstract=&galleyFullText=&discipline=&subject=&type=&coverage=&indexTerms=&dateFromMonth=&dateFromDay=&dateFromYear=&dateToMonth=&dateToDay=&dateToYear=&orderBy=score&orderDir=desc&searchPage={}#results'.format(j+1)
    driver.get(url)
    time.sleep(3)
    for i in range(25):
        try:
            scraper(i)
        except selenium.common.exceptions.NoSuchElementException:
            pass
export_csv = papers.to_csv (r'/home/ordipret2/BerthaFiles/proyectoData/UCR2Results.csv', index = None, header=True) 