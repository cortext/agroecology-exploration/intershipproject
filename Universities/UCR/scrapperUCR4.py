from selenium import webdriver
import selenium
import pandas as pd 
import time 

driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')
url = 'http://kimuk.conare.ac.cr/Search/Results?filter%5B%5D=instname_str%3A%22Universidad+de+Costa+Rica%22&lookfor=agroecologia&type=AllFields'
driver.get(url)

papers = pd.DataFrame(columns = ['Titulo','link','autor','Periodo','formato'])
time.sleep(4)
def scraper(u):
    print(u+1)
    n = '/html/body/div[1]/div/div[1]/div[{}]/div[2]/div/div[2]/a'.format(u+1)
    print(n)
    nombre = driver.find_element_by_xpath(n)
    print(nombre.text)
    p = '/html/body/div[1]/div/div[1]/div[{}]/div[2]/div/div[3]/div[2]'.format(u+1)
    periodo = ''
    try:
        period = driver.find_element_by_xpath(p)
        periodo = period.text
        print(period.text)
    except selenium.common.exceptions.NoSuchElementException:
        pass
    a = '/html/body/div[1]/div/div[1]/div[{}]/div[2]/div/div[3]/div[1]/a'.format(u+1)
    autor = driver.find_element_by_xpath(a)
    print(autor.text)
    l = '/html/body/div[1]/div/div[1]/div[{}]/div[2]/div/div[5]/div/div/a'.format(u+1)
    link = driver.find_element_by_xpath(l)
    print(link.get_attribute('href'))
    papers.loc[len(papers)] = [nombre.text,link.get_attribute('href'),autor.text,periodo,'Articulo']
j = 4
while(j <=6):
    print(j)
    time.sleep(3)
    for i in range(20):
        try: 
            scraper(i)
        except selenium.common.exceptions.NoSuchElementException:
            pass
    time.sleep(1)
    nb = '/html/body/div[1]/div/div[1]/ul/li[{}]/a'.format(j)
    try:
        nextButton4 = driver.find_elements_by_xpath(nb)[0]
        nextButton4.click()
    except selenium.common.exceptions.NoSuchElementException:
        print("ultimos")
        pass
        
    j=j+2
for i in range(20):
    print('ultimos')
    try: 
        scraper(i)
    except selenium.common.exceptions.NoSuchElementException:
        pass
export_csv = papers.to_csv (r'/home/ordipret2/BerthaFiles/proyectoData/UCR4Results.csv', index = None, header=True) 