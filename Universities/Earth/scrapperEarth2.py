from selenium import webdriver
import pandas as pd 
import time 

driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')
driver.get('https://earthac.sharepoint.com/biblioteca/SiteAssets/EBSCO.aspx')

text_area_email = driver.find_element_by_id('i0116')
text_area_email.send_keys("XXXXX")

nextButton = driver.find_elements_by_xpath('//*[@id="idSIButton9"]')[0]
nextButton.click()
time.sleep(1)
text_area_password = driver.find_element_by_id('i0118')
text_area_password.send_keys("XXXX")
time.sleep(1)

nextButton2 = driver.find_element_by_css_selector('#idSIButton9')
nextButton2.click()
time.sleep(1)

nextButton3 = driver.find_elements_by_xpath('//*[@id="idSIButton9"]')[0]
nextButton3.click()
time.sleep(2)
window_login = driver.window_handles[0]

nextButton4 = driver.find_elements_by_xpath('/html/body/form/div[12]/div/div[2]/div[2]/div[3]/table/tbody/tr/td/div/div/div/div/div[1]/table/tbody/tr/td[2]/table/tbody/tr/td/div/strong/a')[0]
nextButton4.click()

time.sleep(2)
window_search = driver.window_handles[1]
driver.switch_to_window(window_search)

time.sleep(4)

text_area_search = driver.find_element_by_id('SearchTerm1')
text_area_search.send_keys('Agroecologia')
nextButton5 = driver.find_elements_by_xpath('//*[@id="SearchButton"]')[0]
nextButton5.click()

time.sleep(4)
def scrapper(r):
    path = '//*[@id="Result_{}"]'.format(r)
    access_botton = driver.find_element_by_xpath(path)
    access_botton.click()
    time.sleep(2)
    export_button = driver.find_element_by_xpath('/html/body/form/div[2]/div[1]/div[5]/div/ul/li[7]/a')
    export_button.click()
    time.sleep(2)
    CSV_option = driver.find_element_by_xpath('//*[@id="saveFormat_ExportToCsv"]')
    CSV_option.click()
    time.sleep(2)
    descargar_button = driver.find_element_by_xpath('/html/body/form/div[2]/div[1]/div[3]/div/div[2]/div/div/div/div[2]/div/input[1]')
    descargar_button.click()
    time.sleep(2)
    back_button = driver.find_element_by_xpath('//*[@id="ctl00_ctl00_MainContentArea_MainContentArea_topNavControl_linkResult"]')
    back_button.click()
    time.sleep(3)
n = 128
for i in range(128):
    nextButton6 = driver.find_elements_by_xpath('//*[@id="ctl00_ctl00_MainContentArea_MainContentArea_bottomMultiPage_lnkNext"]')[0]
    nextButton6.click()

while(n <= 200):
    time.sleep(3)
    for i in range(9):
        print ("n",n)
        e = (i+1)+(10*n)
        print("e",e)
        scrapper(e)
    nextButton6 = driver.find_elements_by_xpath('//*[@id="ctl00_ctl00_MainContentArea_MainContentArea_bottomMultiPage_lnkNext"]')[0]
    nextButton6.click()
    n+=1
