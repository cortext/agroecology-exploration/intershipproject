from selenium import webdriver
import selenium
import pandas as pd 
import time 

papers = pd.DataFrame(columns = ['Periodo','Titulo','autor','link'])
driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')


def scraper(u):
    print(u+1)
    n = '/html/body/div[3]/div[1]/div/div[2]/div[1]/div/div[3]/div[{}]/div[2]/a/h4'.format(u+1)
    print(n)
    nombre = driver.find_element_by_xpath(n)
    print(nombre.text)
    p = '/html/body/div[3]/div[1]/div/div[2]/div[1]/div/div[3]/div[{}]/div[2]/div/span[2]/small/span[2]'.format(u+1)
    periodo = ''
    try:
        period = driver.find_element_by_xpath(p)
        periodo = period.text
        print(period.text)
    except selenium.common.exceptions.NoSuchElementException:
        pass
    a = '/html/body/div[3]/div[1]/div/div[2]/div[1]/div/div[3]/div[{}]/div[2]/div/span[1]/small'.format(u+1)
    autor = driver.find_element_by_xpath(a)
    print(autor.text)
    l = '/html/body/div[3]/div[1]/div/div[2]/div[1]/div/div[3]/div[{}]/div[2]/a'.format(u+1)
    link = driver.find_element_by_xpath(l)
    print(link.get_attribute('href'))
    papers.loc[len(papers)] = [periodo,nombre.text,autor.text,link.get_attribute('href')]
for j in range(5):
    url = "https://repositoriotec.tec.ac.cr/handle/2238/3327/discover?rpp=10&page={}&query=agroecologia&group_by=none&etal=0".format(j+1)
    driver.get(url)
    time.sleep(4)
    for i in range(10):
        try:
            scraper(i)
        except selenium.common.exceptions.NoSuchElementException:
            pass
export_csv = papers.to_csv (r'/home/ordipret2/BerthaFiles/proyectoData/TEC2.csv', index = None, header=True) 